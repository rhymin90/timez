
import { createPinia } from 'pinia'
// import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { useEntryStore } from '~/stores/entryStore'
import type { UserModule } from '~/types'

// Setup Pinia
// https://pinia.esm.dev/
export const install: UserModule = ({ isClient, initialState, app, router }) => {
  const pinia = createPinia()
  // pinia.use(piniaPluginPersistedstate)
  app.use(pinia)
  // Refer to
  // https://github.com/antfu/vite-ssg/blob/main/README.md#state-serialization
  // for other serialization strategies.
  if (isClient) {
    pinia.state.value = (initialState.pinia) || {}
  }
  else {
    initialState.pinia = pinia.state.value
  }

  router.beforeEach((to, from, next) => {
    const store = useEntryStore(pinia)
    if (!store.isReady) {
      // perform the (user-implemented) store action to fill the store's state
      const dateFromHash = to.hash?.substring(1)
      const date = new Date()
      const dateString = dateFromHash || `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
      const storageKey = `store-${dateString}`
      if (isClient) {
        store.initialize(dateString, localStorage.getItem(storageKey))
        store.$subscribe((_, state) => {
          localStorage.setItem(storageKey, JSON.stringify(state))
        })
      }
    }
    next()
  })
}
