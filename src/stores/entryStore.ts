import { acceptHMRUpdate, defineStore } from 'pinia'
import { Entry } from '~/model/entry'

interface EntryState {
  date: string | null
  entries: Entry[] | null
}

export const useEntryStore = defineStore('entry', {
  // persist: true,
  state: (): EntryState => ({
    date: null,
    entries: null,
  }),
  getters: {
    // count: state => state?.entries.size || 0,
    isReady(state) {
      return !!state.entries
    },
  },
  actions: {
    initialize(date: string, json: string | null) {
      if (this.isReady) {
        return
      }

      if (json !== null) {
        console.log('init store with data', json)
        const data = JSON.parse(json) as { date: string; entries: Entry[] }
        this.date = data.date
        this.entries = data.entries.map(it => Entry.deserialize(it))
      }
      else {
        this.date = date
        console.log('init fresh store for date', this.date)
        this.entries = [
          new Entry('Meetings'),
        ]
      }
    },
    addEntry(name: string) {
      if (this.entries && this.entries.findIndex(it => it.name === name) < 0) {
        this.entries?.push(new Entry(name))
      }
    },
    removeEntry(name: string) {
      if (this.entries) {
        const index = this.entries.findIndex(it => it.name === name)
        if (index >= 0) {
          this.entries.splice(index, 1)
        }
      }
    },
  },

})

if (import.meta.hot) { import.meta.hot.accept(acceptHMRUpdate(useEntryStore, import.meta.hot)) }
