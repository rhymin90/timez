export class Entry {
  name: string
  time: number

  constructor(name: string, time?: number) {
    this.name = name
    this.time = time && time > 0 ? time : 0
  }

  public static deserialize(data: { name: string; time: number }): Entry {
    return new Entry(data.name, data.time)
  }
}
